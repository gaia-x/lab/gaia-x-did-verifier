# [1.1.0-development.1](https://gitlab.com/gaia-x/lab/gaia-x-did-verifier/compare/v1.0.0...v1.1.0-development.1) (2024-07-15)


### Features

* expose typings ([be82fde](https://gitlab.com/gaia-x/lab/gaia-x-did-verifier/commit/be82fde3a7143b088844f79df8eef105adfda5cb))

# 1.0.0 (2024-03-12)


### Features

* **CLI:** use main lab registry by default ([aa411f2](https://gitlab.com/gaia-x/lab/gaia-x-did-verifier/commit/aa411f2487d1214b940a8554610130d937175d0e))
* create did-verifier library ([fb65240](https://gitlab.com/gaia-x/lab/gaia-x-did-verifier/commit/fb6524018008747440a815e78b1caf12b569f67d))
* **LAB-485:** did verification library ([e905332](https://gitlab.com/gaia-x/lab/gaia-x-did-verifier/commit/e905332a265e15be5d5bc86c5c5f43f07e891328))
* **LAB-551:** support relative DID URLs ([c3da0fd](https://gitlab.com/gaia-x/lab/gaia-x-did-verifier/commit/c3da0fd5d2089c692321cafd666a69c611e76bed))
* npm release and slack notice ([8484ab5](https://gitlab.com/gaia-x/lab/gaia-x-did-verifier/commit/8484ab5b0ef656431e7ec831b018d4e22c77f137))
* output valid verification methods ([febaad3](https://gitlab.com/gaia-x/lab/gaia-x-did-verifier/commit/febaad3f064fcccfaa39993eb9a05e9c619cb352))

# [1.0.0-development.3](https://gitlab.com/gaia-x/lab/gaia-x-did-verifier/compare/v1.0.0-development.2...v1.0.0-development.3) (2024-03-12)


### Features

* npm release and slack notice ([8484ab5](https://gitlab.com/gaia-x/lab/gaia-x-did-verifier/commit/8484ab5b0ef656431e7ec831b018d4e22c77f137))

# [1.0.0-development.2](https://gitlab.com/gaia-x/lab/gaia-x-did-verifier/compare/v1.0.0-development.1...v1.0.0-development.2) (2024-03-12)


### Features

* output valid verification methods ([febaad3](https://gitlab.com/gaia-x/lab/gaia-x-did-verifier/commit/febaad3f064fcccfaa39993eb9a05e9c619cb352))

# 1.0.0-development.1 (2024-03-12)


### Features

* **CLI:** use main lab registry by default ([aa411f2](https://gitlab.com/gaia-x/lab/gaia-x-did-verifier/commit/aa411f2487d1214b940a8554610130d937175d0e))
* create did-verifier library ([fb65240](https://gitlab.com/gaia-x/lab/gaia-x-did-verifier/commit/fb6524018008747440a815e78b1caf12b569f67d))
* **LAB-485:** did verification library ([e905332](https://gitlab.com/gaia-x/lab/gaia-x-did-verifier/commit/e905332a265e15be5d5bc86c5c5f43f07e891328))
* **LAB-551:** support relative DID URLs ([c3da0fd](https://gitlab.com/gaia-x/lab/gaia-x-did-verifier/commit/c3da0fd5d2089c692321cafd666a69c611e76bed))
