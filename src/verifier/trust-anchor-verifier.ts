/**
 * Represents the result of a trust anchor verification, as a success or failure with a message.
 */
export type TrustAnchorVerificationResult = { success: true } | { success: false; message: string }

/**
 * A service to verify a certificate chain against trust anchors
 */
export interface TrustAnchorVerifier {
  /**
   * Verifies a certificate chain against trust anchors
   * @param certificateChain The certificate chain to verify as a PEM X.509 string
   * @returns A promise that resolves to true if the certificate chain is trusted, false otherwise
   */
  isTrusted(certificateChain: string): Promise<TrustAnchorVerificationResult>
}
