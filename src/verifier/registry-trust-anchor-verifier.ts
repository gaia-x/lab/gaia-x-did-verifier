import { TrustAnchorVerificationResult, TrustAnchorVerifier } from './trust-anchor-verifier'

/**
 * Verifier relying on a registry Web Service to confirm certificate chains have valid trust anchor.
 */
export class RegistryTrustAnchorVerifier implements TrustAnchorVerifier {
  private readonly endpoint: string

  /**
   * @param registryBaseUrl the base URL of a registry web service
   * @param verificationPath the path of a POST endpoint relative to the registryBaseUrl to submit the trust anchor to.
   * It must accept certificate chain body as pem, and return a 2XX if the trust anchor is found, or any error code otherwise.
   * Defaults to `"/api/trustAnchor/chain"`
   */
  constructor(registryBaseUrl: string, verificationPath = '/api/trustAnchor/chain') {
    this.endpoint = registryBaseUrl + verificationPath
  }

  async isTrusted(certificateChain: string): Promise<TrustAnchorVerificationResult> {
    const result = await fetch(this.endpoint, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-pem-file'
      },
      body: certificateChain
    })
    if (result.ok) {
      return { success: true }
    }
    const body = await result.text()
    let message = body
    try {
      const json = JSON.parse(body)
      if (json.error) {
        message = json.error
      } else if (json.message) {
        message = json.message
      }
    } catch {
      // Not a JSON body
    }
    return { success: false, message: message || result.statusText }
  }
}
