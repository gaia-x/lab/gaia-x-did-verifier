import { DidVerificationException } from './exception/did-verification.exception'
import { VerificationError, VerificationOptions, VerificationSummary, VerifierOptions } from './verifier'
import { DIDDocument, VerificationMethod } from 'did-resolver'
import { defaultDidResolver, DidResolver } from '../resolver/did-resolver'
import { CertificateChainResolver, defaultCertificateChainResolver } from '../resolver/certificate-chain-resolver'
import { importJWK, exportSPKI, importX509, KeyLike } from 'jose'
import { RegistryTrustAnchorVerifier } from './registry-trust-anchor-verifier'
import { TrustAnchorVerifier } from './trust-anchor-verifier'

/**
 * In charge of DID verification
 */
export class DidVerifier {
  private readonly didResolver: DidResolver
  private readonly certificateChainResolver: CertificateChainResolver
  private readonly trustAnchorVerifier: TrustAnchorVerifier

  /**
   * Create a new DID Verifier. Each instance may be configured with different resolvers and trust anchor verifier.
   * @param options it may include an optional didVerifier and certificateChainResolver to override default behaviour,
   * and must contain either a trustAnchorVerifier or a registryBaseUrl.
   */
  constructor(options: VerifierOptions) {
    this.didResolver = options.didResolver ?? defaultDidResolver()
    this.certificateChainResolver = options.certificateChainResolver ?? defaultCertificateChainResolver()
    if ('trustAnchorVerifier' in options) {
      this.trustAnchorVerifier = options.trustAnchorVerifier
    } else {
      this.trustAnchorVerifier = new RegistryTrustAnchorVerifier(
        options.registryBaseUrl,
        options.registryVerificationPath ?? '/api/trustAnchor/chain'
      )
    }
  }

  /**
   * Verify a DID, and resolve with the DID document in case of success. In case of failure, it rejects with an error message.
   * This method relies on the {@link getVerificationSummary} to perform its checks.
   * @param did the DID to verify
   * @param options customization of the verification process
   * @returns a promise resolving to the DID document, or a promise rejecting with {@link DidVerificationException} if verification fails.
   * @see {@link getVerificationSummary}
   */
  async verify(did: string, options?: VerificationOptions): Promise<DIDDocument> {
    const result = await this.getVerificationSummary(did, options)
    if (!result.success) {
      throw new DidVerificationException(result.errors)
    }
    return result.didDocument
  }

  /**
   * Perform verifications for a DID. It outputs all verification errors found for the candidate's verification methods.
   * Be aware that this promise never fails, you must check the success flag in the promise output. If you prefer a promise that rejects,
   * use the {@link verify} method instead.
   * @param did the DID to verify
   * @param options customization of the verification process
   * @returns a promise resolving to the verification summary.
   * In case of success, the didDocument property will contain the DID document.
   * In case of failure, the errors property will contain an array of verification errors.
   */
  async getVerificationSummary(did: string, options?: VerificationOptions): Promise<VerificationSummary> {
    let didDocument: DIDDocument
    try {
      didDocument = await this.didResolver.resolve(did)
    } catch (e: any) {
      return { success: false, errors: [{ code: 'did_resolution_error', message: e?.message ?? 'Could not resolve DID' }] }
    }
    const methodsToVerify = this.getMethodsToVerify(didDocument, options)
    if (!methodsToVerify.length) {
      return {
        success: false,
        errors: [
          {
            code: 'missing_verification_method',
            message:
              options && 'verificationMethod' in options
                ? `Verification method ${options.verificationMethod} cannot be found for ${did}`
                : `No verification method exists for ${did}`
          }
        ]
      }
    }
    const errorsByMethod = await Promise.all(
      methodsToVerify.map(async method => ({ method, errors: await this.getVerificationMethodErrors(method) }))
    )
    if (options?.requireValidVerificationMethods === 'atLeastOne') {
      const validMethods = errorsByMethod.filter(r => r.errors.length === 0).map(r => r.method)
      if (validMethods.length) {
        return { success: true, didDocument, methods: validMethods }
      }
    }
    const errors = (<VerificationError[]>[]).concat(...errorsByMethod.map(ebm => ebm.errors))
    return errors.length ? { success: false, errors } : { success: true, didDocument, methods: errorsByMethod.map(ebm => ebm.method) }
  }

  /**
   * Get the verification errors for a given verification method.
   * @param method the verification method to search errors for.
   * @returns a promise resolving to an array of verification errors. If no errors are found, the array will be empty.
   */
  async getVerificationMethodErrors(method: VerificationMethod): Promise<VerificationError[]> {
    const publicKey = method.publicKeyJwk
    if (!publicKey) {
      return [{ code: 'missing_public_key', message: `Public key cannot be found`, verificationMethod: method.id }]
    }
    let certificateChain: string
    try {
      certificateChain = await this.certificateChainResolver.resolve(publicKey.x5u)
    } catch (e: any) {
      return [{ code: 'missing_certificate_chain', message: `Certificate chain cannot be found`, verificationMethod: method.id }]
    }
    return this.getCertificateErrors(publicKey, certificateChain, method.id)
  }

  /**
   * Get the verification errors for a given public key and certificate chain.
   * @param publicKey the public key to verify. It must be a JWK.
   * @param certificateChain the certificate chain to verify. It must be a PEM.
   * @param verificationMethod the verification method to search errors for.
   * @returns a promise resolving to an array of verification errors. If no errors are found, the array will be empty.
   */
  async getCertificateErrors(publicKey: JsonWebKey, certificateChain: string, verificationMethod: string): Promise<VerificationError[]> {
    const errors: VerificationError[] = await this.getCertificateMatchesPublicKeyErrors(publicKey, certificateChain, verificationMethod)
    try {
      const trusted = await this.trustAnchorVerifier.isTrusted(certificateChain)
      if (!trusted.success) {
        errors.push({ code: 'no_trust_anchor', message: `Certificate chain is not trusted. ${trusted.message}`, verificationMethod })
      }
    } catch (e: any) {
      return [{ code: 'trust_anchor_check_failed', message: `Trust anchor verification failed. ${e?.message ?? e}`, verificationMethod }]
    }
    return errors
  }

  /**
   * Check if the public key matches the certificate chain.
   * @param publicKeyJwk the public key to verify. It must be a JWK.
   * @param certificateChain the certificate chain to verify. It must be a PEM.
   * @param verificationMethod the verification method to search errors for.
   * @returns a promise resolving to an array of verification errors. If no errors are found, the array will be empty.
   */
  async getCertificateMatchesPublicKeyErrors(
    publicKeyJwk: JsonWebKey & { [k: string]: any },
    certificateChain: string,
    verificationMethod: string
  ): Promise<VerificationError[]> {
    try {
      const publicKey = await importJWK(publicKeyJwk)
      const publicKeySpki = await exportSPKI(publicKey as KeyLike)

      const x509 = await importX509(certificateChain.trim(), publicKeyJwk.alg ?? 'PS256')
      const x509Spki = await exportSPKI(x509)

      const isOk = publicKeySpki === x509Spki
      return isOk ? [] : [{ code: 'public_key_mismatch', message: `Public key does not match certificate chain`, verificationMethod }]
    } catch (error: any) {
      return [{ code: 'public_key_check_error', message: `Could not verify certificate chain. ${error?.message}`, verificationMethod }]
    }
  }

  /**
   * Get the verification methods to verify based on the given options for a DID document.
   * @param didDocument the DID document to get verification methods from.
   * @param options the options to use to get verification methods.
   * - If options.verificationMethod is a string, it will search for the verification method with the same identifier
   * - If options.verificationMethod is an array of strings, it will be used to filter the verification methods with their id included in the list.
   * @returns the list of verification methods found
   */
  private getMethodsToVerify(didDocument: DIDDocument, options?: VerificationOptions): VerificationMethod[] {
    if (options?.verificationMethod) {
      const acceptanceList = Array.isArray(options.verificationMethod) ? options.verificationMethod : [options.verificationMethod]
      return (didDocument.verificationMethod ?? []).filter(vm => acceptanceList.includes(vm.id) || acceptanceList.includes(didDocument.id + vm.id))
    }
    return didDocument.verificationMethod ?? []
  }
}
