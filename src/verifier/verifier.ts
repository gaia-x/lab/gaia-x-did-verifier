import { DIDDocument, VerificationMethod } from 'did-resolver'
import { CertificateChainResolver } from '../resolver/certificate-chain-resolver'
import { DidResolver } from '../resolver/did-resolver'
import { TrustAnchorVerifier } from './trust-anchor-verifier'

/**
 * Options to customize the verifier class.
 * One of trustAnchorVerifier or registryBaseUrl must be provided.
 */
export type VerifierOptions = {
  /**
   * A custom resolver to use for resolving the DID document. Default to a did:web resolver.
   */
  didResolver?: DidResolver
  /**
   * A custom resolver to use for resolving the certificate chain. Default to basic HTTP text resolver.
   */
  certificateChainResolver?: CertificateChainResolver
} & (
  | {
      /**
       * A web service to verify a certificate chain against trust anchors
       */
      trustAnchorVerifier: TrustAnchorVerifier
    }
  | {
      /**
       * The base URL of a registry web service
       */
      registryBaseUrl: string
      /**
       * The path of a POST endpoint relative to the registryBaseUrl to submit the trust anchor to.
       * It must accept certificate chain body as pem, and return a 2XX if the trust anchor is found,
       * or any error code otherwise.
       * @default "/api/trustAnchor/chain"
       */
      registryVerificationPath?: string
    }
)

/**
 * The result of a DID verification process.
 * If success is true, the DID document is returned.
 * If success is false, the {@link VerificationError} array is returned.
 */
export type VerificationSummary =
  | {
      /**
       * Verification succeeded
       */
      success: true
      /**
       * The DID document we resolved to while verifying the DID
       */
      didDocument: DIDDocument
      /**
       * One or more verification methods that successfully passed verification
       */
      methods: VerificationMethod[]
    }
  | {
      /**
       * Verification failed
       */
      success: false
      /**
       * The list of errors encountered while performing verification
       * @see {@link VerificationError}
       */
      errors: VerificationError[]
    }

/**
 * Represents a DID verification error.
 * - `did_resolution_error`        : DID resolution lead to an error (probably unresolvable).
 * - `missing_verification_method` : No verification method can be found in the DID document matching the given verification options.
 * - `missing_public_key`          : Public key cannot be found in the DID document.
 * - `public_key_mismatch`         : Public key does not match the certificate chain.
 * - `missing_certificate_chain`   : Certificate chain `x5u` resolution lead to an error (probably unresolvable).
 * - `public_key_check_error`      : Error occurred while resolving the certificate chain. Make sure the certificate chain is formatted as a valid X.509
 * - `no_trust_anchor`             : Certificate chain is not trusted by the registry.
 * - `trust_anchor_check_failed`   : Trust anchor verification failed. Depending on its implementation, this does not necessarily imply that the certificate chain is untrusted; it could be due to a temporary unavailability of the service. When using a Gaia-X registry, this error would happen if the verification service is down.
 */
export type VerificationError = {
  /**
   * A human-readable explanation of the error.
   */
  message: string
} & (
  | {
      code: 'did_resolution_error' | 'missing_verification_method'
    }
  | {
      code:
        | 'missing_public_key'
        | 'public_key_mismatch'
        | 'public_key_check_error'
        | 'missing_certificate_chain'
        | 'no_trust_anchor'
        | 'trust_anchor_check_failed'
      /**
       * The verification method id which caused the error
       */
      verificationMethod: string
    }
)

/**
 * Options to customize the verification process.
 */
export interface VerificationOptions {
  /**
   * The verification method id(s) to search for
   * - If a string, it will search for the verification method with the same identifier
   * - If an array of strings, it will be used to filter the verification methods with their id included in the list.
   * - If undefined, all verification methods form the DID document will be checked
   */
  verificationMethod?: string | string[]
  /**
   * If 'all', the verification method must be present in the DID document.
   * If 'atLeastOne', the verification method must not be present in the DID document.
   * If undefined, will default to 'all'
   * @default 'all'
   */
  requireValidVerificationMethods?: 'all' | 'atLeastOne'
}
