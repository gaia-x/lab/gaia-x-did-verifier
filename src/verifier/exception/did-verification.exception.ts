import { VerificationError } from '../verifier'

/**
 * Indicates a DID verification has failed. Error details are available under the {@link errors} property.
 * Error message is a concatenation of all {@link errors} messages.
 */
export class DidVerificationException extends Error {
  constructor(
    /**
     * List of errors that caused the DID verification failure
     */
    public readonly errors: VerificationError[]
  ) {
    super(errors.map(e => `${e.code}: ${e.message}`).join(' \n'))
  }
}
