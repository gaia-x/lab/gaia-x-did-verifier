/**
 * A service to resolve certificate chains
 */
export interface CertificateChainResolver {
  /**
   * Resolves a certificate chain from a URL
   * @param url The URL of the certificate chain to resolve
   */
  resolve(url: string): Promise<string>
}

/**
 * @returns a resolver to fetch the certificate chain from the internet
 */
export function defaultCertificateChainResolver(): CertificateChainResolver {
  return {
    async resolve(url: string) {
      return (await fetch(url)).text()
    }
  }
}
