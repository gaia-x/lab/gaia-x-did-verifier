export class DidResolutionException extends Error {
  constructor(message: string) {
    super(message)
  }
}
