import { DIDDocument, Resolver } from 'did-resolver'
import { DidResolutionException } from './exception/did-resolution.exception'
import { getResolver } from 'web-did-resolver'

/**
 * A service to resolve DID documents.
 */
export interface DidResolver {
  /**
   * Resolve a DID to its DID document.
   * @param did the `did:method:example` to resolve
   */
  resolve(did: string): Promise<DIDDocument>
}

/**
 * @returns a did:web resolver fetching from the Internet
 */
export function defaultDidResolver(): DidResolver {
  const resolver = new Resolver({
    ...getResolver()
  })
  return {
    async resolve(did: string) {
      try {
        const result = await resolver.resolve(did)
        if (result.didResolutionMetadata.error) {
          throw new DidResolutionException(result.didResolutionMetadata.error)
        }
        if (!result.didDocument) {
          throw new DidResolutionException('DID document is missing')
        }
        return result.didDocument
      } catch (e: any) {
        throw new DidResolutionException(e?.message || 'Could not resolve DID')
      }
    }
  }
}
