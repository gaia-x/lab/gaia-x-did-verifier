#!/usr/bin/env node

import { DidVerifier } from './verifier'

const [, , did, verificationMethod, registryBaseUrl] = process.argv

if (!did) {
  console.error("Usage: <did> [<verificationMethod> | '*'] [<registryBaseUrl>]")
  process.exit(-1)
}

new DidVerifier({ registryBaseUrl: registryBaseUrl || 'https://registry.lab.gaia-x.eu/main' })
  .getVerificationSummary(did, verificationMethod && verificationMethod !== '*' ? { verificationMethod } : {})
  .then(summary => {
    if (summary.success) {
      console.log('[OK] ' + summary.didDocument.id)
    } else {
      for (const err of summary.errors) {
        console.error('[KO] ' + err.code + ': ' + err.message)
      }
      process.exit(-2)
    }
  })
