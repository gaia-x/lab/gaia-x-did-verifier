import { TrustAnchorVerifier } from '../../verifier/trust-anchor-verifier'
import trustAnchors from './trust-anchors'

const trustAnchorVerifier: TrustAnchorVerifier = {
  isTrusted: (certificateChain: string) => {
    if (!certificateChain) {
      return Promise.reject('Invalid certificate chain')
    }
    const rootCert = (certificateChain.split('-----END CERTIFICATE-----')[0] + '-----END CERTIFICATE-----').replace(/\s|\n/g, '')
    const valid = trustAnchors.includes(rootCert)
    return valid ? Promise.resolve({ success: true }) : Promise.resolve({ success: false, message: 'No trust anchor' })
  }
}

export default trustAnchorVerifier
