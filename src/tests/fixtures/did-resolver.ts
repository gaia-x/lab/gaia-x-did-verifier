import { DIDDocument, JsonWebKey, VerificationMethod } from 'did-resolver'
import { DidResolver } from '../../resolver/did-resolver'

const basePublicKey: JsonWebKey = {
  alg: 'PS256',
  kty: 'RSA',
  n: 'o3SgmQr5MyjWcRUCYGPp0P5dUDJw9VPRCGrwwdCiRQPe7zPf41hwEP-UhvJM1lHLr6H5uQInNxylXmooRPjbJ60xkSrNGLcjCyQxsZj3JorH3FjbUYmLoZBKH8WUn5u3lFpnySD4d2toz84aiWZPOkzmpaU2tfU5qUB9y-Urp7VVl_GDCDnubfOkzCpQEe_lCs0rRXT1yOeVqnof00pAywbq6xr4SFVln24lublFeiUrclH4u1tLq0EVoKrB7WCPvi7FbbEe0II1uUwfEDR90J60h6MOsqByijOmCSSWLTQyXX-UmZ_T-2aL646PJ2SKOhS8_OmVjLecn0j4X-LrOw',
  e: 'AQAB',
  x5u: 'https://example.x590.pem'
}

const baseVerificationMethod: Omit<VerificationMethod, 'id'> = {
  type: 'JsonWebKey2020',
  controller: 'did:example:foo/bar',
  publicKeyJwk: basePublicKey
}

const dids: any = {
  'did:example:foo/bar': {
    document: <DIDDocument>{
      id: 'did:example:foo/bar',
      verificationMethod: [
        {
          ...baseVerificationMethod,
          id: 'did:example:foo/bar#JWK2020'
        },
        {
          ...baseVerificationMethod,
          id: '#relativeMethod'
        },
        {
          ...baseVerificationMethod,
          id: 'notInTrustAnchor',
          publicKeyJwk: {
            ...basePublicKey,
            n: 'yitOrHemzVsRA0MZjvEFUtTPjfUTKlNbUZtwYKqsVYrbyu-7qE53PEVsOiS5RtJSA2cEcGJ7IhnOFnFPhrKH_x9cAkW_cYhQP5dWEcBd8B-SGzjjDSqA_BinYNrbRk3Z_c57EHtdmrzWdAnXBC427lN5OEI1ZDNJxit5s--KX5_yV_WzTqEB7ZPfParF8lnmkEIjy_qnZ_KsKGV3cu4EhIRtd5J77jV8WWYPj2VpkGQmTEO7IjINFyrEEJqYRrl42d35-27h-pAPGAtEwGiloWZnL-gqt8Z9JIQZgig3pmBbwBrgX8rHsHpycz_vIoasMCoc2u3AL4nNv0Q4ZyJMIw',
            x5u: 'https://exampleNoTrustAnchor.x590.pem'
          }
        },
        {
          ...baseVerificationMethod,
          id: 'invalidKeyMethod',
          publicKeyJwk: {
            ...basePublicKey,
            n: 'notMatchingPublicKey'
          }
        },
        {
          id: 'nonResolvableCertificateChain',
          type: 'JsonWebKey2020',
          publicKeyJwk: {
            ...basePublicKey,
            x5u: 'https://oups.itdoesntexist'
          }
        },
        {
          id: 'missingCertificateChain',
          type: 'JsonWebKey2020',
          publicKeyJwk: {
            alg: 'PS256',
            kty: 'RSA',
            n: 'invalidPublicKey',
            e: 'AQAB'
          }
        },
        {
          id: 'publicKeyMissing'
        }
      ]
    }
  },
  'did:example:empty': {
    document: <DIDDocument>{
      id: 'did:example:empty',
      verificationMethod: []
    }
  }
}

const didResolver: DidResolver = {
  resolve: (did: string) => Promise.resolve(dids[did].document)
}

export default didResolver
