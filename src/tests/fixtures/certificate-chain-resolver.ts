import { CertificateChainResolver } from '../..'
import x509TrustAnchor from './x509-trust-anchor'
import x509NoTrustAnchor from './x509-no-trust-anchor'

const x509: { [url: string]: string } = {
  'https://example.x590.pem': x509TrustAnchor,
  'https://exampleNoTrustAnchor.x590.pem': x509NoTrustAnchor
}

const certificateChainResolver: CertificateChainResolver = {
  resolve: (url: string) => Promise.resolve(x509[url])
}

export default certificateChainResolver
