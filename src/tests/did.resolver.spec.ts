import { DIDDocument } from 'did-resolver'
import { HttpResponse, http } from 'msw'
import { setupServer } from 'msw/node'
import { afterAll, afterEach, beforeAll, describe, expect, it } from 'vitest'
import { defaultDidResolver, DidResolver } from '../resolver/did-resolver'
import { DidResolutionException } from '../resolver/exception/did-resolution.exception'

describe('DidResolver', () => {
  const server = setupServer(
    http.get('https://example.com/issuer/123/did.json', () =>
      HttpResponse.json({
        context: [
          'https://www.w3.org/ns/did/v1',
          'https://w3id.org/security/suites/jws-2020/v1',
          {
            '@base': 'https://example.com/issuer/123'
          }
        ],
        id: 'did:web:example.com:issuer:123',
        publicKey: [
          {
            id: '#ovsDKYBjFemIy8DVhc-w2LSi8CvXMw2AYDzHj04yxkc',
            type: 'JsonWebKey2020',
            controller: 'did:web:example.com:issuer:123',
            publicKeyJwk: {
              kty: 'OKP',
              crv: 'Ed25519',
              x: 'CV-aGlld3nVdgnhoZK0D36Wk-9aIMlZjZOK2XhPMnkQ'
            }
          }
        ],
        assertionMethod: ['#ovsDKYBjFemIy8DVhc-w2LSi8CvXMw2AYDzHj04yxkc']
      })
    ),
    http.get(
      'https://unresolvable.com/.well-known/did.json',
      () =>
        new HttpResponse(null, {
          status: 404
        })
    ),
    http.get(
      'https://failing.com/.well-known/did.json',
      () =>
        new HttpResponse(null, {
          status: 500
        })
    )
  )
  const didResolver: DidResolver = defaultDidResolver()

  beforeAll(() => server.listen())
  afterEach(() => server.resetHandlers())
  afterAll(() => server.close())

  it('should resolve a valid DID web', async () => {
    const didDocument: DIDDocument = await didResolver.resolve('did:web:example.com:issuer:123')

    expect(didDocument.id).toEqual('did:web:example.com:issuer:123')
    expect(didDocument.assertionMethod).toHaveLength(1)
    expect(didDocument.assertionMethod![0]).toEqual('#ovsDKYBjFemIy8DVhc-w2LSi8CvXMw2AYDzHj04yxkc')
  })

  it.each(['', null, undefined, 'did:web:unresolvable.com', 'did:web:failing.com', 'did:key:z6MkhaXgBZDvotDkL5257faiztiGiC2QtKLGpbnnEGta2doK'])(
    'should throw a DidResolutionException',
    async (failingDid: string | null | undefined) => {
      try {
        // @ts-ignore
        await didResolver.resolve(failingDid)
      } catch (e) {
        expect(e).toBeInstanceOf(DidResolutionException)
      }
    }
  )
})
