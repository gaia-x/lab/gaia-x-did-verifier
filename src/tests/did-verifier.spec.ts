import { beforeAll, describe, expect, it } from 'vitest'
import { DidVerifier } from '../index'
import certificateChainResolver from './fixtures/certificate-chain-resolver'
import didResolver from './fixtures/did-resolver'
import trustAnchorVerifier from './fixtures/trust-anchor-verifier'

describe('did-verifier', () => {
  let verifier: DidVerifier

  beforeAll(() => {
    verifier = new DidVerifier({ trustAnchorVerifier, certificateChainResolver, didResolver })
  })

  it('valid DID should pass verification', async () => {
    const did = 'did:example:foo/bar'
    const summary = await verifier.getVerificationSummary(did, { verificationMethod: 'did:example:foo/bar#JWK2020' })
    expect(summary).toMatchObject({ success: true })
  })
  it('valid DID should ouput single method passing validation', async () => {
    const did = 'did:example:foo/bar'
    const summary = await verifier.getVerificationSummary(did, { verificationMethod: 'did:example:foo/bar#JWK2020' })
    expect(summary.success).toBeTruthy()
    if (summary.success) {
      expect(summary.methods.map(m => m.id)).toStrictEqual(['did:example:foo/bar#JWK2020'])
    }
  })
  it('valid DID should ouput multiple methods passing validation', async () => {
    const did = 'did:example:foo/bar'
    const summary = await verifier.getVerificationSummary(did, { requireValidVerificationMethods: 'atLeastOne' })
    expect(summary.success).toBeTruthy()
    if (summary.success) {
      expect(summary.methods.map(m => m.id).sort()).toStrictEqual(['#relativeMethod', 'did:example:foo/bar#JWK2020'])
    }
  })
  it('valid DID with relative verification method ID should pass verification', async () => {
    const did = 'did:example:foo/bar'
    const summary = await verifier.getVerificationSummary(did, { verificationMethod: 'did:example:foo/bar#relativeMethod' })
    expect(summary).toMatchObject({ success: true })
  })
  it('valid DID with at least one valid verification method should pass verification with "atLeastOne" requirement', async () => {
    const did = 'did:example:foo/bar'
    const summary = await verifier.getVerificationSummary(did, { requireValidVerificationMethods: 'atLeastOne' })
    expect(summary).toMatchObject({ success: true })
  })
  it('valid DID with at least one invalid verification method should not pass verification with "all" requirement', async () => {
    const did = 'did:example:foo/bar'
    const summary = await verifier.getVerificationSummary(did, { requireValidVerificationMethods: 'all' })
    expect(summary).toMatchObject({ success: false })
  })
  it('valid DID verification should return DID document', async () => {
    const did = 'did:example:foo/bar'
    const document = await verifier.verify(did, { verificationMethod: 'did:example:foo/bar#JWK2020' })
    expect(document).toMatchObject({ id: did })
  })
  it('invalid DID verification should throw an error', async () => {
    const did = 'did:not:existing'
    await expect(verifier.verify(did, { verificationMethod: 'did:example:foo/bar#JWK2020' })).rejects.toThrowError(/did_resolution_error/)
  })
  it('unresolvable DID verification method should fail', async () => {
    const did = 'did:not:existing'
    const summary = await verifier.getVerificationSummary(did)
    expect(summary).toMatchObject({ success: false, errors: [{ code: 'did_resolution_error' }] })
  })
  it('valid DID with no verification method should fail', async () => {
    const did = 'did:example:empty'
    const summary = await verifier.getVerificationSummary(did)
    expect(summary).toMatchObject({ success: false, errors: [{ code: 'missing_verification_method' }] })
  })
  it('valid DID with non existing verification method should fail', async () => {
    const did = 'did:example:foo/bar'
    const summary = await verifier.getVerificationSummary(did, { verificationMethod: 'iDoNotExist' })
    expect(summary).toMatchObject({ success: false, errors: [{ code: 'missing_verification_method' }] })
  })
  it('valid DID with valid verification method and invalid public key should fail', async () => {
    const did = 'did:example:foo/bar'
    const summary = await verifier.getVerificationSummary(did, { verificationMethod: 'invalidKeyMethod' })
    expect(summary).toMatchObject({ success: false, errors: [{ code: 'public_key_mismatch' }] })
  })
  it('valid DID with valid verification method and missing public key should fail', async () => {
    const did = 'did:example:foo/bar'
    const summary = await verifier.getVerificationSummary(did, { verificationMethod: 'publicKeyMissing' })
    expect(summary).toMatchObject({ success: false, errors: [{ code: 'missing_public_key' }] })
  })
  it('should fail on certificate chain resolve failure', async () => {
    const did = 'did:example:foo/bar'
    const failVerifier = new DidVerifier({ trustAnchorVerifier, certificateChainResolver: { resolve: () => Promise.reject('fail') }, didResolver })
    const summary = await failVerifier.getVerificationSummary(did, { verificationMethod: 'missingCertificateChain' })
    expect(summary).toMatchObject({ success: false, errors: [{ code: 'missing_certificate_chain' }] })
  })
  it('valid DID with valid verification method and non resolvable certificate chain should fail', async () => {
    const did = 'did:example:foo/bar'
    const summary = await verifier.getVerificationSummary(did, { verificationMethod: 'nonResolvableCertificateChain' })
    expect(summary).toMatchObject({ success: false, errors: [{ code: 'trust_anchor_check_failed' }] })
  })
  it('valid DID with valid verification method and certificate chain with no trust anchor should fail', async () => {
    const did = 'did:example:foo/bar'
    const summary = await verifier.getVerificationSummary(did, { verificationMethod: 'notInTrustAnchor' })
    expect(summary).toMatchObject({ success: false, errors: [{ code: 'no_trust_anchor' }] })
  })
})
