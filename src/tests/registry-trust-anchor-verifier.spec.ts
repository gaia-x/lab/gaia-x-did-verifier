import { http, HttpResponse } from 'msw'
import { setupServer } from 'msw/node'
import { afterAll, afterEach, beforeAll, describe, expect, it } from 'vitest'
import { RegistryTrustAnchorVerifier } from '../index'
import certificate from './fixtures/certificate-no-trust-anchor'

describe('registry-trust-anchor-verifier', () => {
  const server = setupServer(
    http.post('https://success.trust-anchor-verifier.eu/verify', () =>
      HttpResponse.json({
        success: true
      })
    ),
    http.post(
      'https://json-failure.trust-anchor-verifier.eu/verify',
      () => new HttpResponse('{"message": "No trust anchor found"}', { status: 409 })
    ),
    http.post(
      'https://raw-failure.trust-anchor-verifier.eu/verify',
      () =>
        new HttpResponse('Verification failed', {
          status: 500
        })
    )
  )

  beforeAll(() => server.listen())
  afterEach(() => server.resetHandlers())
  afterAll(() => server.close())

  it('lab development registry verifier should inform that certificate chain is not from a trusted anchor', async () => {
    const verifier = new RegistryTrustAnchorVerifier('https://registry.lab.gaia-x.eu/development')
    const check = await verifier.isTrusted(certificate)
    expect(check).toMatchObject({ success: false, message: 'Unable to validate cert chain root certificate: Root certificate not trusted.- Conflict' })
  })
  it('verifier should inform that certificate chain with trusted anchor is valid', async () => {
    const verifier = new RegistryTrustAnchorVerifier('https://success.trust-anchor-verifier.eu', '/verify')
    const check = await verifier.isTrusted(certificate)
    expect(check).toMatchObject({ success: true })
  })
  it('verifier should inform that certificate chain with no trusted anchor is invalid (JSON body)', async () => {
    const verifier = new RegistryTrustAnchorVerifier('https://json-failure.trust-anchor-verifier.eu', '/verify')
    const check = await verifier.isTrusted(certificate)
    expect(check).toMatchObject({ success: false, message: 'No trust anchor found' })
  })
  it('verifier should inform that certificate chain with no trusted anchor is invalid (raw body)', async () => {
    const verifier = new RegistryTrustAnchorVerifier('https://raw-failure.trust-anchor-verifier.eu', '/verify')
    const check = await verifier.isTrusted(certificate)
    expect(check).toMatchObject({ success: false, message: 'Verification failed' })
  })
})
