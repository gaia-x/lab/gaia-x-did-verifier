# Gaia-X AISBL DID Verifier Library

![Coverage](https://gitlab.com/gaia-x/lab/gaia-x-did-verifier/badges/main/coverage.svg)
![NPM Version](https://img.shields.io/npm/v/%40gaia-x%2Fgaia-x-did-verifier)
![NPM Downloads](https://img.shields.io/npm/dt/%40gaia-x%2Fgaia-x-did-verifier)
![NPM bundle size](https://img.shields.io/bundlephobia/min/%40gaia-x%2Fgaia-x-did-verifier)
![GitLab License](https://img.shields.io/gitlab/license/gaia-x%2Flab%2Fgaia-x-did-verifier)

> A JavaScript library to verify DIDs and their verification methods against a registry

This library allows you to verify a [DID](https://www.w3.org/TR/did-core/) against a trust anchor:

- It checks that DID resolves to a document
- It ensures the DID document contains a verification method with a x5u field
- It verifies the x5u matches the publicJwk
- It checks the certificate is trusted by a registry

## Requirements

This library is designed to be compatible with Node version 18+.

The main requirement to run this library is to have a Typescript enabled project.

## Command line interface

Not familiar with code? This library can be used as a CLI tool.

```bash
npm install -g @gaia-x/did-verifier
verify-did did:web:foo.bar
```

Usage: `verify-did <did> [<verificationMethod> | '*'] [<registryBaseUrl>]`

- `verificationMethod` defaults to any match `*`
- `registryBaseUrl` defaults to `https://registry.lab.gaia-x.eu/main`

## Getting Started

Install this package and use it anywhere in your project.

```bash
npm install @gaia-x/did-verifier
```

After installation, you can import the DidVerifier class from the package and use it to verify DIDs against trust anchors in your application. Here's a basic example of how to use the library:

```typescript
import { DidVerifier } from '@gaia-x/did-verifier'

const verifier = new DidVerifier({ registryBaseUrl: 'https://registry.lab.gaia-x.eu/development' })
verifier
  .verify('did:web:example')
  .then(() => {
    console.log('DID contains a trusted verification method')
  })
  .catch(e => {
    console.error('Not passing', e)
  })
```

Replace `did:web:example` with the DID you want to verify. Adjust the options as needed for your verification process.

This is the most straightforward scenario. In real-world use-cases, you may find it necessary to tailor the verification process to your specific requirements or expand it to accommodate other DID methods, or to check a specific verification method from the DID document. For further customization options and additional functionality, please continue with the documentation provided below.

## DID verifier configuration

You can configure the DID verifier with the following options:

| Option name              | Description                                                                                                                                                                                                          | Default value                                                         |
| ------------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :-------------------------------------------------------------------- |
| didResolver              | The resolver to get a DID document from a DID.                                                                                                                                                                       | A did:web resolver fetching from the Internet                         |
| certificateChainResolver | The resolver used to get certificate chain.                                                                                                                                                                          | Will fetch the certificate chain from the internet                    |
| trustAnchorVerifier      | A web service to verify a certificate chain against trust anchors.                                                                                                                                                   | None - either trustAnchorVerifier or registryBaseUrl must be provided |
| registryBaseUrl          | The base URL of a registry web service.                                                                                                                                                                              | None - either registryBaseUrl or trustAnchorVerifier must be provided |
| registryVerificationPath | The path of a POST endpoint relative to the registryBaseUrl to submit the trust anchor to. It must accept certificate chain body as pem, and return a 2XX if the trust anchor is found, or any error code otherwise. | "/api/trustAnchor/chain"                                              |

## DID verification options

You can customize the verification process according to your needs. These options include:

| Option name                     | Description                                                                                                                                                                                    |
| ------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| verificationMethod              | You can pass either a string or an array of strings matching the verification method identifiers to look for in the DID document. If not specified every verification method will be verified. |
| requireValidVerificationMethods | "all": All defined/given verification methods must be valid ; "atLeastOne": At least one of the defined/given verification methods must be valid                                               |

## Verification result

There is two way to pass validation:

- Call the `verify` method to get a DIDDocument promise as result or an rejected promise with failure description
- Call the `getVerificationSummary` method which returns a promise containing a success flag along with the DIDDocument in case of success, or a false success flag accompanied by a list of errors in case of failure, each identified with specific code. This approach allows fine-tuned error management without promise rejection.

The possible error codes are listed below:

| Error Code                    | Description                                                                                                                                                                                                                                                                                          |
| ----------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `did_resolution_error`        | DID resolution lead to an error (probably unresolvable).                                                                                                                                                                                                                                             |
| `missing_verification_method` | No verification method can be found in the DID document matching the given verification options.                                                                                                                                                                                                     |
| `missing_public_key`          | Public key cannot be found in the DID document.                                                                                                                                                                                                                                                      |
| `public_key_mismatch`         | Public key does not match the certificate chain.                                                                                                                                                                                                                                                     |
| `missing_certificate_chain`   | Certificate chain `x5u` resolution lead to an error (probably unresolvable).                                                                                                                                                                                                                         |
| `public_key_check_error`      | Error occurred while resolving the certificate chain. Make sure the certificate chain is formatted as a valid X.509                                                                                                                                                                                  |
| `no_trust_anchor`             | Certificate chain is not trusted by the registry.                                                                                                                                                                                                                                                    |
| `trust_anchor_check_failed`   | Trust anchor verification failed. Depending on its implementation, this does not necessarily imply that the certificate chain is untrusted; it could be due to a temporary unavailability of the service. When using a Gaia-X registry, this error would happen if the verification service is down. |

It is also possible to perform intermediate verification steps directly using `getVerificationMethodErrors`, `getCertificateErrors` and `getCertificateMatchesPublicKeyErrors`

## Examples

### Validate a verification method against Trust Anchors

**Objective**: Verify a specific verification method associated with a DID comes from a trusted anchor.

```typescript
const verifier = new DidVerifier({ registryBaseUrl: 'https://registry.lab.gaia-x.eu/development' })
verifier
  .verify('did:web:example', {
    verificationMethod: 'did:web:example#key1'
  })
  .then(() => {
    console.log('Given verification method is trusted')
  })
  .catch(e => {
    console.error('Not passing', e)
  })
```

### Validate some verification methods against Trust Anchors

**Objective**: Validate that any or all verification methods associated with a DID or namely identified originate from a trusted anchor.

```typescript
// Verify one method can be trusted
verifier
  .verify('did:web:example', {
    requireValidVerificationMethods: 'atLeastOne'
  })
  .then(() => {
    console.log('Found a trusted verification method')
  })

// Verify all methods can be trusted
verifier
  .verify('did:web:example', {
    requireValidVerificationMethods: 'all'
  })
  .then(didDoc => {
    console.log('All verification methods can be trusted', didDoc.verificationMethod?.map(m => m.id))
  })

// Verify all given methods can be trusted
verifier
  .verify('did:web:example', {
    verificationMethod: ['did:web:example#key1', 'did:web:example#key2']
    requireValidVerificationMethods: 'all' // can be atLeastOne to check if any of #key1 or #key2 is trustable
  })
  .then(() => {
    console.log('All verification methods can be trusted')
  })
```
